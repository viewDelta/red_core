package com.red.ct.utility;

import com.red.ct.configuration.key.AppChannelConfig;
import com.red.ct.configuration.key.KeyMappingConfig;
import com.red.ct.exception.InvalidHeaderValueException;
import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.constants.Header;
import com.red.ct.utility.constants.RequestType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HeaderValidation {

    @Autowired
    private KeyMappingConfig keyMappingConfig;

    @Autowired
    private AppChannelConfig appChannelConfig;

    /**
     * This will validate the request header based on request Type
     * @param headers {@link HttpHeaders}
     * @param requestType {@link RequestType}
     * @throws Exception
     */
    public void validateRequestHeader(HttpHeaders headers, RequestType requestType) throws Exception {
        log.debug("Entering HeaderValidation.validateRequestHeader method");
        String xChannel = headers.getFirst(Header.X_CHANNEL.getValue());
        String xApiKey = headers.getFirst(Header.X_API_KEY.getValue());
        validateXChannelWithKey(xChannel, xApiKey, requestType.getRequestType());
        log.debug("Exiting HeaderValidation.validateRequestHeader method");
    }

    /**
     * This will validate, given x-api-key is valid w.r.t to x-channel or not
     * @param xChannel {@link String}
     * @param xApiKey {@link String}
     * @param requestType {@link String}
     * @throws InvalidHeaderValueException
     */
    private void validateXChannelWithKey(String xChannel, String xApiKey, String requestType) throws InvalidHeaderValueException {
        if(StringUtils.isNotBlank(xApiKey) && StringUtils.isNotBlank(xChannel)
                && (appChannelConfig.containsRequestTypeAndXChannel(requestType, xChannel)
                && keyMappingConfig.containsXChannelAndKey(xChannel, xApiKey))) {
                return;
            }
        throw new InvalidHeaderValueException(ApplicationMessage.INVALID_OR_MISSING_API_KEY_OR_XCHANNEL_COMBINATION);
    }
}
