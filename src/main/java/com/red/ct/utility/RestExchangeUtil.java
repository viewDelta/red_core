package com.red.ct.utility;

import com.red.ct.domain.outbound.request.IRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@Service
public class RestExchangeUtil {

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public <T> CompletableFuture<ResponseEntity<T>> execute(String url, HttpMethod method,
                                                                HttpHeaders headers, IRequest payload, Class<T> type) throws InterruptedException {
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<IRequest> requestEntity = new HttpEntity<>(payload, headers);
        ResponseEntity<T> result = restTemplate.exchange(url, method, requestEntity, type);
        return CompletableFuture.completedFuture(result);
    }
}
