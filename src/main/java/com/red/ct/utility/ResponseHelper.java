package com.red.ct.utility;

import com.red.ct.domain.inbound.response.ApiResponse;
import com.red.ct.utility.constants.ApplicationCode;
import com.red.ct.utility.constants.Constant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResponseHelper {

    private ResponseHelper(){}

    /**
     * This will return the created or request processed success message
     * @param status {@link HttpStatus} 200 or 201 depending upon which value will be generated
     * @return {@link ResponseEntity<ApiResponse>} Api response for the api
     */
    public static final ResponseEntity<ApiResponse> buildSuccessResponse(HttpStatus status){
        ResponseEntity<ApiResponse> response = null;
        ApiResponse obj = new ApiResponse(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
        		ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg());
        if(status.value() == 200) {
            response = new ResponseEntity<>(obj, HttpStatus.OK);
        } else if (status.value() == 201){
            response = new ResponseEntity<>(obj, HttpStatus.CREATED);
        }
        return response;
    }
    
    /**
     * This will return the corresponding error code value from error description
     * Currently, only IN is supported, same can be found in src/main/resource folder
     * @param errMsg {@link String}
     * @return {@link String}
     */
    public static final String getCodeFromMsg(final String errMsg) {
    	ResourceBundle bundle = ResourceBundle.getBundle(Constant.ERROR_MESSAGES , new Locale("en", "IN"));
    	Enumeration<String> keys = bundle.getKeys();
    	String key;
    	while(keys.hasMoreElements()) {
    		key = keys.nextElement();
    		if(StringUtils.equalsIgnoreCase(errMsg, bundle.getString(key))){
    			return key;
    		}
    	}
    	return null;
    }
    
}
