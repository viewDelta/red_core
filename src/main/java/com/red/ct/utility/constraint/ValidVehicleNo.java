package com.red.ct.utility.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.validator.VehicleNumberValidator;

@Documented
@Constraint(validatedBy = VehicleNumberValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidVehicleNo {

	String message() default ApplicationMessage.INVALID_OR_MISSING_RC;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
