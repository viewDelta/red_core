package com.red.ct.utility.constraint;

import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.validator.LicenseValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = LicenseValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidLicense {
    String message() default ApplicationMessage.INVALID_OR_MISSING_LICENSE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
