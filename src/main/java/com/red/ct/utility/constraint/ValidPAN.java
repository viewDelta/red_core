package com.red.ct.utility.constraint;

import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.validator.PANValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PANValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPAN {
	
	String message() default ApplicationMessage.INVALID_OR_MISSING_PAN;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
