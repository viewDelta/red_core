package com.red.ct.utility.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.validator.PinCodeValidator;

@Documented
@Constraint(validatedBy = PinCodeValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPincode {
	String message() default ApplicationMessage.INVALID_OR_MISSING_PINCODE;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
