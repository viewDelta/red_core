package com.red.ct.utility.constraint;


import com.red.ct.utility.constants.ApplicationMessage;
import com.red.ct.utility.validator.NameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NameValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidName {
    String message() default ApplicationMessage.INVALID_OR_MISSING_NAME;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
