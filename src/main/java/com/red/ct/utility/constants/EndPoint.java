package com.red.ct.utility.constants;

public class EndPoint {
	
	private EndPoint(){}
	
    public static final String LOGIN = "/login";
    public static final String LOGIN_CHECK = "/check";
	public static final String SIGNUP = "/signup";
    public static final String CLIENT = "/client";
    public static final String OUTLET = "/outlet";
    public static final String TRANSPORT = "/transport";
}
