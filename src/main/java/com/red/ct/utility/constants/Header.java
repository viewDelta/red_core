package com.red.ct.utility.constants;

import lombok.Getter;

@Getter
public enum Header {

    AUTHORIZATION("AUTHORIZATION"),
    X_CHANNEL("X-CHANNEL"),
    X_API_KEY("X-API-KEY"),
    X_CORRELATION_ID("X-CORRELATION-ID");

    private final String value;
    private Header(String value){
        this.value = value;
    }
}
