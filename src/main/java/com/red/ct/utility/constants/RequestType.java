package com.red.ct.utility.constants;

import lombok.Getter;

@Getter
public enum RequestType {

    LOGIN("LOGIN"),
    LOGIN_CHECK("LOGIN_CHECK"),
    SIGNUP_FOR_OUTLET("SIGNUP_FOR_OUTLET"),
    SIGNUP_FOR_DELIVERY_PERSON("SIGNUP_FOR_DELIVERY_PERSON"),
    SIGNUP_FOR_CONSUMER("SIGNUP_FOR_CONSUMER"),
    GET_OUTLETS("GET_OUTLETS");


    private final String requestType;

    private RequestType(String requestType){
        this.requestType = requestType;
    }
}
