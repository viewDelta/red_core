package com.red.ct.utility.constants;

public enum TokenClaim {

    AUTHORITY("AUTHORITY"),
    PHONE_NO("phoneNo"),
    X_CHANNEL("X-CHANNEL"),
    JWT_SUBJECT("RED_CORE");

    private String claimKey;

    private TokenClaim(String claimKey) {
        this.claimKey = claimKey;
    }

    public String getKey() {
        return claimKey;
    }
}
