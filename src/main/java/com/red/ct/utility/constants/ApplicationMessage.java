package com.red.ct.utility.constants;

public class ApplicationMessage {
	
	private ApplicationMessage() {}
	
	// ApplicationCode Error Messages
	public static final String REQUEST_PROCESSED_SUCCESSFULLY = "Request processed successfully";
	public static final String INVALID_PAYLOAD = "Invalid payload received";
	public static final String INTERNAL_SERVER_ERROR = "Internal server error";
	public static final String INVALID_OR_MISSING_PHONE_NUMBER = "Invalid or missing phone number";
	public static final String INVALID_OR_MISSING_NAME = "Invalid or missing name";
	public static final String INVALID_OR_MISSING_EMAIL = "Invalid or missing email";
	public static final String INVALID_OR_MISSING_DATE = "Invalid or missing date";
	public static final String INVALID_OR_MISSING_PINCODE = "Invalid or missing pin code";
	public static final String INVALID_OR_MISSING_STATECODE = "Invalid or missing state code";
	public static final String INVALID_OR_MISSING_COUNTRYCODE = "Invalid or missing country code";
	public static final String INVALID_OR_MISSING_AADHAAR_NUMBER = "Invalid or missing aadhaar number";
	public static final String INVALID_OR_MISSING_PAN = "Invalid or missing PAN";
	public static final String INVALID_OR_MISSING_RC = "Invalid or missing RC";
	public static final String INVALID_OR_MISSING_LICENSE = "Invalid or missing license";
	public static final String INVALID_OR_MISSING_API_KEY_OR_XCHANNEL_COMBINATION = "Invalid or missing X-API-KEY or X-CHANNEL";

	// Invalid Payload Error messages
	public static final String ENTRY_ALREADY_EXISTS = "Entity can't be created";
	public static final String DUPLICATE_LICENCE_OR_RC = " licence number or rc is already registered";
	public static final String DUPLICATE_PHONE_OR_EMAIL = " phone number or email is already registered";
	public static final String USER_NOT_FOUND_OR_INACTIVE = "user not found or inactive";
	public static final String INVALID_PHONE_NUMBER_AND_COUNTRY_CODE_COMBINATION = "Invalid phone number and country code combination";
	public static final String INVALID_REF_ID_AND_OTP_COMBINATION = "Invalid reference id or otp combination";

}
