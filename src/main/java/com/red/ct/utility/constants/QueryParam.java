package com.red.ct.utility.constants;

import lombok.Getter;

@Getter
public class QueryParam {

    private QueryParam() {}

    public final static String PIN_CODE = "pincode";
    public final static String LONGITUDE = "longitude";
    public final static String LATITUDE = "latitude";

}
