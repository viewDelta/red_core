package com.red.ct.utility.constants;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * This will contain all the state codes, will be used for vehicle or user validation
 */
@Getter
public enum StateCode {

    AN("AN"),
    AP("AP"),
    AR("AR"),
    AS("AS"),
    BR("BR"),
    CH("CH"),
    CG("CG"),
    DD("DD"),
    DL("DL"),
    GA("GA"),
    GJ("GJ"),
    HR("HR"),
    HP("HP"),
    JK("JK"),
    JH("JH"),
    KA("KA"),
    KL("KL"),
    LA("LA"),
    LD("LD"),
    MP("MP"),
    MH("MH"),
    MN("MN"),
    ML("ML"),
    MZ("MZ"),
    NL("NL"),
    OD("OD"),
    PY("PY"),
    PB("PB"),
    RJ("RJ"),
    SK("SK"),
    TN("TN"),
    TS("TS"),
    TR("TR"),
    UP("UP"),
    UK("UK"),
    WB("WB");

    private final String code;

    private StateCode(String code){
        this.code = code;
    }

    /**
     * This will check if, this has valid state codes
     * @param code {@link String}
     * @return {@link Boolean}
     */
    public static boolean isValidCode(String code){
        boolean flag = false;
        if(StringUtils.isNotBlank(code)) {
            for (StateCode stateCode : StateCode.values()) {
                if (StringUtils.equalsIgnoreCase(stateCode.getCode(), code)) {
                    flag = true;
                }
            }
        }
        return flag;
    }
}
