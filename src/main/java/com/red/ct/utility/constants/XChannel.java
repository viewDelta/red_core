package com.red.ct.utility.constants;

import lombok.Getter;

/**
 * This contains xChannel values for which request will be processed
 */
@Getter
public enum XChannel {
	
	BUYER("BUYER"),
	TRANSPORT("TRANSPORT"),
	SELLER("SELLER");
	
	private String value;
	
	private XChannel(String value) {
		this.value = value;
	}

}
