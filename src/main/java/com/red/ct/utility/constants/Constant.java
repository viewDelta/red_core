package com.red.ct.utility.constants;

public class Constant {

	private Constant() {}

	public static final String ERROR_MESSAGES = "ErrorMessages";
	public static final String CORRELATION_ID= "CorrelationId";
	public static final String BEARER = "Bearer ";
	public static final String DISTANCE_UNIT_FORMAT = "#.0#"; //Distance unit format, upto 2 decimal prevision

	// Indicates sms api related information
	public static final String SMS_FROM = "D7sms";
	public static final String OTP_MSG = "<#> {0} is your {1} verification code. Valid for {2} minutes. Do not share OTP for security reasons.";
	public static final int OTP_LENGTH = 4;
}
