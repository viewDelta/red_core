package com.red.ct.utility.constants;

import lombok.Getter;

/**
 * This enum will have all the details of the api response codes
 */
@Getter
public enum ApplicationCode {
	
	/**
	 * {@code RED-000 Request processed successfully}
	 */
	REQUEST_PROCESSED_SUCCESSFULLY("RED-000", ApplicationMessage.REQUEST_PROCESSED_SUCCESSFULLY),
	
	/**
	 * {@code RED-001 Invalid payload received}
	 */
	INVALID_PAYLOAD("RED-001", ApplicationMessage.INVALID_PAYLOAD),

	/**
	 * {@code RED-002 Internal server error}
	 */
	INTERNAL_SERVER_ERROR("RED-002", ApplicationMessage.INTERNAL_SERVER_ERROR),
	
	
	/**
	 * {@code RED-003 Invalid or missing phone number}
	 */
	INVALID_OR_MISSING_PHONE_NUMBER("RED-003", ApplicationMessage.INVALID_OR_MISSING_PHONE_NUMBER),
	

	/**
	 * {@code RED-004 Invalid or missing name}
	 */
	INVALID_OR_MISSING_NAME("RED-004",ApplicationMessage.INVALID_OR_MISSING_NAME),
	
	/**
	 * {@code RED-005 Invalid or missing EMAIL}
	 */
	INVALID_OR_MISSING_EMAIL("RED-005", ApplicationMessage.INVALID_OR_MISSING_EMAIL),
	
	/**
	 * {@code RED-006 Invalid or missing date}
	 */
	INVALID_OR_MISSING_DATE("RED-006", ApplicationMessage.INVALID_OR_MISSING_DATE),
	
	/**
	 * {@code RED-007 Invalid or missing pin code}
	 */
	INVALID_OR_MISSING_PINCODE("RED-007", ApplicationMessage.INVALID_OR_MISSING_PINCODE),
	
	/**
	 * {@code RED-008 Invalid or missing aadhaar number}
	 */
	INVALID_OR_MISSING_AADHAAR_NUMBER("RED-008", ApplicationMessage.INVALID_OR_MISSING_AADHAAR_NUMBER),
	
	/**
	 * {@code RED-009 Invalid or missing PAN}
	 */
	INVALID_OR_MISSING_PAN("RED-009", ApplicationMessage.INVALID_OR_MISSING_PAN),
	
	/**
	 * {@code RED-010 Invalid or missing RC}
	 */
	INVALID_OR_MISSING_RC("RED-010", ApplicationMessage.INVALID_OR_MISSING_RC),
	
	/**
	 * {@code RED-011 Invalid or missing license}
	 */
	INVALID_OR_MISSING_LICENSE("RED-011", ApplicationMessage.INVALID_OR_MISSING_LICENSE),
	
	/**
	 * {@code RED-012 Invalid or missing X-API-KEY or X-CHANNEL}
	 */
	INVALID_OR_MISSING_API_KEY_OR_X_CHANNEL("RED-012", ApplicationMessage.INVALID_OR_MISSING_API_KEY_OR_XCHANNEL_COMBINATION),

	/**
	 * {@code RED-013 Invalid or missing Authentication Token}
	 */
	INVALID_OR_MISSING_AUTH_TOKEN("RED-013", "Invalid or missing Authentication Token"),

	/**
	 * {@code RED-014 Duplicate Entry found}
	 */
	DUPLICATE_ENTRY_FOUND("RED-014", "Duplicate entry found with the input data"),

	/**
	 * {@code RED-015 No record found}
	 */
	NO_RECORD_FOUND("RED-015", "No record found"),
	/**
	 * {code RED-016 Invalid Query Parameter.}
	 */
	INVALID_QUERY_PARAM("RED-016", "Invalid or missing query parameter");

	private final String code, msg;

	private ApplicationCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
