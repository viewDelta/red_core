package com.red.ct.utility.constants;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum CountryCode {

    IN("IN");

    private final String countryCode;

    private CountryCode(String countryCode){
        this.countryCode = countryCode;
    }

    public static boolean isValidCode(String code){
        boolean flag = false;
        if(StringUtils.isNotBlank(code)) {
            for (CountryCode countryCode : CountryCode.values()) {
                if (StringUtils.equalsIgnoreCase(countryCode.getCountryCode(), code)) {
                    flag = true;
                }
            }
        }
        return flag;
    }
}
