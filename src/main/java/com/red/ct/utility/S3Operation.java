package com.red.ct.utility;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;

@Slf4j
@Component
public class S3Operation {

    /**
     * This will upload file to S3 bucket
     * @param bucketPath {@link String} Describes the full bucketPath where the file will be uploaded
     * @param filePathWithLocation {@link String} This the local file reference which will be uploaded to s3
     * @param s3client {@link AmazonS3} Amazon client reference post region and access configuration if any, to upload file
     * @throws Exception {@link Exception} will be thrown, if there is an issue while uploading the file
     */
    public void uploadFile(String bucketPath, String filePathWithLocation, AmazonS3 s3client) throws Exception{
        log.debug("Entering S3Operation.uploadFile method");
        if (StringUtils.isEmpty(filePathWithLocation) || StringUtils.isEmpty(bucketPath) || null == s3client) {
            log.warn("Invalid parameter received for s3 upload");
            return;
        }
        try {
            File file = new File(filePathWithLocation);
            log.info("Trying to upload file - {} to bucket path - {}", file.getName(), bucketPath);
            s3client.putObject(new PutObjectRequest(bucketPath, file.getName(), file));
            log.info("File has been uploaded to s3");
        } catch (Exception e){
            log.error("Exception occured while uploading file to s3 - {}", e.getMessage());
            throw e;
        }
        log.debug("Exiting S3Operation.uploadFile method");
    }
}
