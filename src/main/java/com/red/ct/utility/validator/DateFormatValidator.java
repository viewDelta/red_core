package com.red.ct.utility.validator;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateFormatValidator {

	private static final String dateFormatRegex = "dd-MM-yyyy HH:mm:ss";
	private DateFormatValidator(){}

	/**
	 * This will validate the given date in format and will return the Date Instance for the same
	 * @param dob {@link String}
	 * @return {@link Date}
	 */
	public static final Date getDateFromString(String dob) {
		Date parsedDate = null;
		if (StringUtils.isNotBlank(dob)) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(dateFormatRegex);
				// To make strict date format validation
				formatter.setLenient(false);
				parsedDate = formatter.parse(dob);
			} catch (ParseException e){
				log.error("Invalid DOB - {} received", dob);
			}
		}
		return parsedDate;
	}
}
