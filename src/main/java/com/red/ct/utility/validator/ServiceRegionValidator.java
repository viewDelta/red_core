package com.red.ct.utility.validator;

import com.red.ct.data.dao.IServiceLocationDAO;
import com.red.ct.data.model.ServiceLocation;
import com.red.ct.utility.constants.CountryCode;
import com.red.ct.utility.constants.StateCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceRegionValidator {

    @Autowired
    private IServiceLocationDAO serviceLocationDAO;

    /**
     * Method to check whether the location comes under service region.
     * @param countryCode
     * @param stateCode
     * @param pinCode
     * @return
     */
    public boolean checkForServiceRegion(String countryCode, String stateCode, String pinCode)  {
        pinCode = StringUtils.trimToEmpty(pinCode);
        if(CountryCode.isValidCode(countryCode) && StateCode.isValidCode(stateCode) && StringUtils.isNotBlank(pinCode)){
            ServiceLocation location = serviceLocationDAO.findByCountryCodeAndStateCode(countryCode, stateCode);
            if(null != location && location.getServicePinCodes().contains(pinCode)){
                return true;
            }
        }
        return false;
    }
}
