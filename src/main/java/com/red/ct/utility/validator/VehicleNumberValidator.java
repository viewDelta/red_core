package com.red.ct.utility.validator;

import com.red.ct.utility.constants.StateCode;
import com.red.ct.utility.constraint.ValidVehicleNo;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class VehicleNumberValidator implements ConstraintValidator<ValidVehicleNo, String> {

	private final static String vehicleNoRegex = "^[A-Z]{2}[ ][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$";
	
	private static final Set<String> allStateCode = new HashSet<>();
	
	static {
		// Deprecated
		allStateCode.add("OR");
		allStateCode.add("UA");
		allStateCode.add("DN");
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean result = false;
		if (StringUtils.isNotBlank(value)) {
			Pattern pattern = Pattern.compile(vehicleNoRegex);
			if(pattern.matcher(value.toUpperCase()).matches()
					&& (allStateCode.contains(value.substring(0,2)) || StateCode.isValidCode(value.substring(0,2)))){
				result = true;
			}
		}
		return result;
	}

}
