package com.red.ct.utility.validator;

import com.red.ct.utility.constraint.ValidPincode;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PinCodeValidator implements ConstraintValidator<ValidPincode, String> {

	private final static String pinCodeRegex = "^[1-9][0-9]{5}$";
	
	/**
	 * This will validate the pinCode is valid or not
	 * 
	 * @param pinCode {@link String} pinCode value
	 * @return {@link Boolean} Result of validation
	 */
	@Override
	public boolean isValid(String pinCode, ConstraintValidatorContext context) {
		boolean result = false;
		if (StringUtils.isNotBlank(pinCode)) {
			Pattern pattern = Pattern.compile(pinCodeRegex);
			result = pattern.matcher(pinCode).matches();
		}
		return result;
	}
}
