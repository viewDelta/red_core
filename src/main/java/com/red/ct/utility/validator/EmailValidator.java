package com.red.ct.utility.validator;

import com.red.ct.utility.constraint.ValidEmail;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

    private final static String emailRegex= "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z]+)\\.([a-zA-Z]{2,3})$";

    /**
     * Implements the validation logic.
     * The state of {@code value} must not be altered.
     * <p>
     * This method can be accessed concurrently, thread-safety must be ensured
     * by the implementation.
     *
     * @param value   object to validate
     * @param context context in which the constraint is evaluated
     * @return {@code false} if {@code value} does not pass the constraint
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean result = false;
        if (StringUtils.isNotBlank(value)) {
            Pattern pattern = Pattern.compile(emailRegex);
            result = pattern.matcher(value.toUpperCase()).matches();
        }
        return result;
    }
}
