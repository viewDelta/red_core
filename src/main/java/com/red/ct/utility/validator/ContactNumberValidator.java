package com.red.ct.utility.validator;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ContactNumberValidator {

	private final PhoneNumberUtil phoneUtil;

	public ContactNumberValidator(){
		phoneUtil = PhoneNumberUtil.getInstance();
	}

	/**
	 * This will validate if the given phone number is valid or not
	 * 
	 * @param phoneNo {@link String} Mobile Number to be validated
	 * @return {@link Boolean} Result of validation
	 */
	public boolean isValid(String phoneNo, String countryCode) {
		log.debug("Entering ContactNumberValidator.isValid() method");
		Phonenumber.PhoneNumber numberProto;
		if(StringUtils.isBlank(StringUtils.trimToEmpty(phoneNo)) ||
				StringUtils.isBlank(StringUtils.trimToEmpty(countryCode))){
			return false;
		}
		try {
			numberProto = phoneUtil.parse(phoneNo, countryCode);
		} catch (NumberParseException e) {
			log.warn("Invalid phoneNo - {} received with countryCode - {}", phoneNo, countryCode);
			return false;
		}
		log.debug("Exiting ContactNumberValidator.isValid() method");
		return phoneUtil.isValidNumberForRegion(numberProto, countryCode);
	}

}
