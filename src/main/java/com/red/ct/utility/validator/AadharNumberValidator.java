package com.red.ct.utility.validator;

import com.red.ct.utility.constraint.ValidAadhar;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class AadharNumberValidator implements ConstraintValidator<ValidAadhar, String>{

	private final static String aadharRegex = "[0-9]{12}";
	
	/**
	 * This will validate if the given aadharCard number is valid or not
	 * 
	 * @param value {@link String} Aadhar Number to be validated, with trimmed white spaces
	 * @return {@link Boolean} Result of validation
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean result = false;
		if (StringUtils.isNotBlank(value)) {
			Pattern pattern = Pattern.compile(aadharRegex);
			result = pattern.matcher(value).matches();
		}
		return result;
	}

}
