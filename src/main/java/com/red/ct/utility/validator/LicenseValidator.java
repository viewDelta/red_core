package com.red.ct.utility.validator;

import com.red.ct.utility.constraint.ValidLicense;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class LicenseValidator implements ConstraintValidator<ValidLicense, String> {

    private final static String licenseRegex = "^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$";

    /**
     * Implements the validation logic.
     * The state of {@code value} must not be altered.
     * <p>
     * This method can be accessed concurrently, thread-safety must be ensured
     * by the implementation.
     *
     * @param value   object to validate
     * @param context context in which the constraint is evaluated
     * @return {@code false} if {@code value} does not pass the constraint
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean result = false;
        if (StringUtils.isNotBlank(value)) {
            Pattern pattern = Pattern.compile(licenseRegex);
            result = pattern.matcher(value.toUpperCase()).matches();
        }
        return result;
    }
}