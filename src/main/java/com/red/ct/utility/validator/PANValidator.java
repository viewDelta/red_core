package com.red.ct.utility.validator;

import com.red.ct.utility.constraint.ValidPAN;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PANValidator implements ConstraintValidator<ValidPAN, String>{

	private final static String panRegex = "^[A-Z]{5}[0-9]{4}[A-Z]{1}$";
	
	/**
	 * This will validate if the given PAN number is valid or not
	 * 
	 * @param value {@link String} PAN Number to be validated
	 * @return {@link Boolean} Result of validation
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean result = false;
		if (StringUtils.isNotBlank(value)) {
			Pattern pattern = Pattern.compile(panRegex);
			result = pattern.matcher(value.toUpperCase()).matches();
		}
		return result;
	}

}
