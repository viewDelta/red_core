package com.red.ct.utility.jwt;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.model.Base;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.utility.constants.TokenClaim;
import com.red.ct.utility.constants.XChannel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JwtOperation {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Autowired
    private ICustomerDetailDAO customerDetailDAO;

    @Autowired
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Autowired
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    /**
     * This will return the token from the user
     * @param user {@link Base} User details for which token is required
     * @return {@link String} Returns the jwt Token for the user
     */
    public String getTokenForUser(Base user) {
        log.debug("Entering JwtOperation.getTokenForUser() method");
        String token = null;
        if(StringUtils.isNotBlank(user.getToken())){
            token = user.getToken();
        } else {
            Claims claims = Jwts.claims().setSubject(TokenClaim.JWT_SUBJECT.getKey());
            claims.put(TokenClaim.AUTHORITY.getKey(), user.getName());
            claims.put(TokenClaim.PHONE_NO.getKey(), user.getPhoneNo());
            if(user instanceof CustomerDetail){
                claims.put(TokenClaim.X_CHANNEL.getKey(), XChannel.BUYER.getValue());
            } else if (user instanceof DeliveryPersonDetail){
                claims.put(TokenClaim.X_CHANNEL.getKey(), XChannel.TRANSPORT.getValue());
            } else {
                claims.put(TokenClaim.X_CHANNEL.getKey(), XChannel.SELLER.getValue());
            }
            token = Jwts.builder()
                    .setClaims(claims)
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .compact();
        }
        log.debug("Exiting JwtOperation.getTokenForUser() method");
        return token;
    }

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token {@link String} the JWT token to parse
     * @return {@link Base} the User object extracted from specified token or null if a token is invalid.
     */
    public Base getUser(String token){
        log.debug("Entering JwtOperation.getUser() method");
        Base user = null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody();
            String xChannel = (String) body.get(TokenClaim.X_CHANNEL.getKey());
            String phoneNo = (String) body.get(TokenClaim.PHONE_NO.getKey());
            if(StringUtils.equalsAnyIgnoreCase(XChannel.BUYER.getValue(), xChannel)){
                user = customerDetailDAO.findByPhoneNo(phoneNo);
            } else if(StringUtils.equalsAnyIgnoreCase(XChannel.TRANSPORT.getValue(), xChannel)){
                user = deliveryPersonDetailDAO.findByPhoneNo(phoneNo);
            } else {
                user = restaurantDetailDAO.findByPhoneNo(phoneNo);
            }
        } catch (JwtException | ClassCastException e) {
            log.error("Invalid Token passed : {}", e.getMessage());
        }
        log.debug("Exiting JwtOperation.getUser() method");
        return user;
    }
}
