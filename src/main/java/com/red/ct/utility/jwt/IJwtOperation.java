package com.red.ct.utility.jwt;

import com.red.ct.data.model.Base;
import com.red.ct.exception.InvalidTokenException;
import org.springframework.http.HttpHeaders;

public interface IJwtOperation {

    /**
     * This will return the token from the user
     * @param user {@link Base} User details for which token is required
     * @return {@link String} Returns the jwt Token for the user
     */
    String getTokenForUser(final Base user);

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param headers {@link HttpHeaders} the JWT token to parse
     * @return {@link Base} the User object extracted from specified token or null if a token is invalid.
     */
    Base getUser(final HttpHeaders headers) throws InvalidTokenException;
}
