package com.red.ct.utility.jwt;

import com.red.ct.data.model.Base;
import com.red.ct.exception.InvalidTokenException;
import com.red.ct.utility.constants.ApplicationCode;
import com.red.ct.utility.constants.Constant;
import com.red.ct.utility.constants.Header;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProxyJwt implements IJwtOperation {

    private final JwtOperation jwtOperation;

    @Autowired
    public ProxyJwt(JwtOperation jwtOperation){
        this.jwtOperation = jwtOperation;
    }

    @Override
    public String getTokenForUser(Base user) {
        log.debug("Entering ProxyJwt.getTokenForUser method");
        String token = null;
        if(null == user)
            log.warn("Invalid user instance has been passed");
        else
            token = jwtOperation.getTokenForUser(user);
        log.debug("Exiting ProxyJwt.getTokenForUser method");
        return token;
    }

    @Override
    public Base getUser(HttpHeaders headers) throws InvalidTokenException {
        if(null != headers && StringUtils.isNotEmpty(StringUtils.trimToEmpty(Header.AUTHORIZATION.getValue()))){
            String token = headers.getFirst(Header.AUTHORIZATION.getValue()).replace(Constant.BEARER, "");
            return jwtOperation.getUser(token);
        }
        log.error("Token expected but found to be empty");
        throw new InvalidTokenException(ApplicationCode.INVALID_OR_MISSING_AUTH_TOKEN.getMsg());
    }

}
