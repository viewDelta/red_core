package com.red.ct.exception;

import lombok.Getter;

/**
 * InvalidDateFormatException will be thrown for invalid or missing date
 * @author abinash
 *
 */
@Getter
public class InvalidDateFormatException extends Exception {

	private static final long serialVersionUID = 8529239045315320428L;

	private final String message;
	
	public InvalidDateFormatException(String message) {
		super(message);
		this.message = message;
	}
}
