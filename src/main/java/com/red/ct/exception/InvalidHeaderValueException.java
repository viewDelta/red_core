package com.red.ct.exception;

import lombok.Getter;

/**
 * This exception is thrown when there are missing or invalid Api-Key
 * @author abinash
 *
 */
@Getter
public class InvalidHeaderValueException extends Exception {

    private static final long serialVersionUID = 7447031756165358988L;

    private final String message;

    public InvalidHeaderValueException(String message) {
        super(message);
        this.message = message;
    }
}
