package com.red.ct.exception;

import lombok.Getter;

/**
 * NoRecordFoundException will be thrown, if no such record can be found with input data
 */
@Getter
public class NoRecordFoundException extends Exception {

    private static final long serialVersionUID = -7657712277675212563L;
    private final String message;

    public NoRecordFoundException(String message){
        super(message);
        this.message = message;
    }

}
