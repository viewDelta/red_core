package com.red.ct.exception;

import lombok.Getter;

/**
 * This exception will be thrown where we get Invalid or missing query parameter.
 */
@Getter
public class InvalidQueryParamException extends Exception{

    private static final long serialVersionUID = -4942518952862975097L;

    private final String message;

    public InvalidQueryParamException(String message) {
        super(message);
        this.message = message;
    }
}
