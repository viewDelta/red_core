package com.red.ct.exception;

import lombok.Getter;

/**
 * This Exception is thrown for invalid or missing token
 */
@Getter
public class InvalidTokenException extends Exception {

    private final String message;

    public InvalidTokenException(String message) {
        super(message);
        this.message = message;
    }
}
