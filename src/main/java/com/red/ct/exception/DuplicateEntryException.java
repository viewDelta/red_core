package com.red.ct.exception;

import lombok.Getter;

/**
 * DuplicateEntryException for handling Duplicate entry in the db.
 */

@Getter
public class DuplicateEntryException extends Exception {

    private static final long serialVersionUID = 2054300131957834632L;

    private final String message;

    public DuplicateEntryException(String message)
    {
        super(message);
        this.message = message;
    }
}
