package com.red.ct.exception;

import lombok.Getter;

/**
 * This exception will be thrown, if given payload is invalid
 * @author abinash
 *
 */
@Getter
public class InvalidPayloadException extends Exception {

	private static final long serialVersionUID = 1148909481655273100L;
	
	private final String message;

	public InvalidPayloadException(String message) {
		super(message);
		this.message = message;
	}

}
