package com.red.ct.exception;

import lombok.Getter;

/**
 * This exception is thrown when there are missing headers
 * @author abinash
 *
 */
@Getter
public class InvalidXChannelException extends Exception {
	private static final long serialVersionUID = 6952031262165358988L;

	private final String message;
	
	public InvalidXChannelException(String message) {
		super(message);
		this.message = message;
	}
}
