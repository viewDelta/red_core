package com.red.ct.domain.inbound.request;

import com.google.gson.Gson;
import com.red.ct.utility.constraint.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
@Data
public class CreateRestaurantRequest implements IRequest {

    @NotNull
    private String restaurantName;

    @NotNull
    private Location location;

    private String phoneNo;

    @ValidEmail
    private String email;

    @ValidPAN
    private String pan;

    private String landmark;

    @NotNull
    private String address;

    @ValidName
    private String name;

    @ValidPincode
    private String pinCode;

    @NotNull
    private String stateCode;

    @NotNull
    private String countryCode;

    @Override
    public String toString(){
        return "CreateRestaurantRequest - " + new Gson().toJson(this);
    }
}
