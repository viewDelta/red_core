package com.red.ct.domain.inbound.request;

import com.google.gson.Gson;
import com.red.ct.utility.constraint.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CreateConsumerRequest implements IRequest {

    private String dob;

    private String phoneNo;

    @NotNull
    private String name;

    @ValidEmail
    private String email;

    private List<Label> labels;

    @NotNull
    private String stateCode;

    @NotNull
    private String countryCode;

    @ValidPincode
    private String pinCode;

    @Override
    public String toString(){
        return "CreateConsumerRequest - " + new Gson().toJson(this) ;
    }
}
