package com.red.ct.domain.inbound.response;

import lombok.Data;

import java.util.List;

@Data
public class GetOutDetailsResponse implements IResponse{

    private List<GetOutletDetails> restaurantList;
}
