package com.red.ct.domain.inbound.response;

import lombok.Data;

/**
 * Return response for get outlet details request.
 */
@Data
public class GetOutletDetails {

    private String id;
    private String restaurantName;
    private String distance;
    private String rating;
}
