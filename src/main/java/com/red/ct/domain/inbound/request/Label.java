package com.red.ct.domain.inbound.request;

import javax.validation.constraints.NotNull;

import com.red.ct.utility.constraint.ValidPincode;

import lombok.Data;

@Data
public class Label {
	
	@NotNull
    private String key;
    
	@NotNull
	private String address;
    
	@NotNull
	private String landmark;
    
	@ValidPincode
	private String pinCode;
    
	private Location location;
}
