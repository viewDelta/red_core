package com.red.ct.domain.inbound.request;

import lombok.Data;

@Data
public class Location {
    private double longitude;
    private double latitude;
}
