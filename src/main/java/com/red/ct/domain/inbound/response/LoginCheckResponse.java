package com.red.ct.domain.inbound.response;

import com.google.gson.Gson;
import lombok.Data;

@Data
public class LoginCheckResponse implements  IResponse{

    private String reqId;

    @Override
    public String toString(){
        return new Gson().toJson(this);
    }
}
