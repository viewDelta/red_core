package com.red.ct.domain.inbound.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {
    
	private String code;
    private String msg;
    private IResponse data;
    private List<ApiResponse> errors;
    
    public ApiResponse(String code, String msg, List<ApiResponse> errors) {
    	this.code = code;
    	this.msg = msg;
    	this.errors = errors;
    }
    
    public ApiResponse(String code, String msg, IResponse data) {
    	this.code = code;
    	this.msg = msg;
    	this.data = data;
    }
    
    public ApiResponse(String code, String msg) {
    	this.code = code;
    	this.msg = msg;
    }
}
