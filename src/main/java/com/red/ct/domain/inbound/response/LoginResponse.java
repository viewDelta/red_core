package com.red.ct.domain.inbound.response;

import java.time.Instant;
import java.util.Date;

import lombok.Data;

@Data
public class LoginResponse implements IResponse{
	
	private String token;
	private Instant createdAt;
}
