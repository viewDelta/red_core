package com.red.ct.domain.inbound.request;

import com.google.gson.Gson;
import com.red.ct.utility.constraint.*;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class  CreateDeliveryPersonRequest implements IRequest {

	@ValidAadhar
	private String aadharNo;

	@ValidEmail
	private String email;
	
	@ValidPAN
    private String PAN;

    @ValidVehicleNo
    private String RC;

    @ValidLicense
    private String licence;

    private String phoneNo;

    @ValidName
    private String name;

    @ValidPincode
    private String pinCode;

    @NotNull
    private String stateCode;

    @NotNull
    private String countryCode;

    @Override
    public String toString(){
        return "CreateDeliveryPersonRequest - " + new Gson().toJson(this) ;
    }
}
