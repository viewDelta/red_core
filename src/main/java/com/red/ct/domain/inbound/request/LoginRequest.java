package com.red.ct.domain.inbound.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"phoneNo",
		"countryCode",
		"refId",
		"OTP"
})
public class LoginRequest implements IRequest {

	@JsonProperty("phoneNo")
	public String phoneNo;

	@JsonProperty("countryCode")
	public String countryCode;

	@JsonProperty("refId")
	public String refId;

	@JsonProperty("OTP")
	public String OTP;

	@Override
	public String toString(){
		return "LoginRequest - " + new Gson().toJson(this) ;
	}
	
}
