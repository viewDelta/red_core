package com.red.ct.domain.outbound.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "to",
        "content",
        "from"
})
public class D7SendSmsRequest implements IRequest {

    @JsonProperty("to")
    public String to;

    @JsonProperty("content")
    public String content;

    @JsonProperty("from")
    public String from;

    @Override
    public String toString(){
        return new Gson().toJson(this);
    }
}
