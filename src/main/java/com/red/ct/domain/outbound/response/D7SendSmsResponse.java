package com.red.ct.domain.outbound.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "message"
})
public class D7SendSmsResponse implements IResponse{

    @JsonProperty("message")
    public String message;

    @Override
    public String toString(){
        return new Gson().toJson(this);
    }
}
