package com.red.ct.configuration.key;

import lombok.Data;

import java.util.List;

@Data
public class KeyMap {
    private String channel;
    private List<String> keys;
}
