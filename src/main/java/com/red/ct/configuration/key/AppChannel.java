package com.red.ct.configuration.key;

import lombok.Data;

import java.util.List;

@Data
public class AppChannel {
    private String requestType;
    private List<String> channels;
}
