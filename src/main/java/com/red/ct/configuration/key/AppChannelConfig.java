package com.red.ct.configuration.key;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "app-channel")
public class AppChannelConfig {

    private List<AppChannel> requestchannels;
    private Map<String, List<String>> appKeyMappingMap;

    @PostConstruct
    public void init(){
        appKeyMappingMap = new HashMap<>();
        for(AppChannel appChannel : requestchannels){
            appKeyMappingMap.put(appChannel.getRequestType().toUpperCase(), appChannel.getChannels());
        }
    }

    public List<AppChannel> getKeyMapping() {
        return requestchannels;
    }

    public void setRequestchannels(List<AppChannel> requestchannels) {
        this.requestchannels = requestchannels;
    }


    /**
     * This will validate if, given RequestType and XChannel are valid combination
     * @param requestType {@link String}
     * @param xChannel {@link String}
     * @return {@link Boolean}
     */
    public boolean containsRequestTypeAndXChannel(String requestType, String xChannel){
        boolean flag = false;
        if(StringUtils.isNotBlank(requestType) && StringUtils.isNotBlank(xChannel)) {
            List<String> allowedXChannel = appKeyMappingMap.get(requestType.toUpperCase());
            if (null != allowedXChannel && allowedXChannel.contains(xChannel.toUpperCase())) {
                flag = true;
            }
        }
        return flag;
    }
}
