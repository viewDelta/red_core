package com.red.ct.configuration.key;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "key-mapping")
public class KeyMappingConfig {

    private List<KeyMap> channelKeys;
    private Map<String, List<String>> keyMappingMap;

    @PostConstruct
    public void init(){
        keyMappingMap = new HashMap<>();
        for(KeyMap keyMap : channelKeys){
            keyMappingMap.put(keyMap.getChannel().toUpperCase(), keyMap.getKeys());
        }
    }

    public List<KeyMap> getKeyMapping() {
        return channelKeys;
    }

    public void setChannelKeys(List<KeyMap> channelKeys) {
        this.channelKeys = channelKeys;
    }

    /**
     * This will validate the xChannel and apiKey combination
     * @param xChannel {@link String}
     * @param apiKey {@link String}
     * @return {@link Boolean}
     */
    public boolean containsXChannelAndKey(String xChannel, String apiKey){
        boolean flag = false;
        if(StringUtils.isNotBlank(xChannel) && StringUtils.isNotBlank(apiKey)) {
            List<String> apiKeys = keyMappingMap.get(xChannel.toUpperCase());
            if(null != apiKeys && apiKeys.contains(apiKey.trim())){
                flag = true;
            }
        }
        return flag;
    }
}
