package com.red.ct.configuration;

import com.red.ct.utility.jwt.JwtOperation;
import com.red.ct.utility.constants.EndPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private JwtOperation jwtOperation;

    @Autowired
    public SecurityConfig(JwtOperation jwtOperation) {
        this.jwtOperation = jwtOperation;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .mvcMatchers(EndPoint.LOGIN, EndPoint.LOGIN_CHECK ,EndPoint.SIGNUP+EndPoint.CLIENT,
                        EndPoint.SIGNUP+EndPoint.TRANSPORT, EndPoint.SIGNUP+EndPoint.OUTLET).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new JwtAuthenticationFilter(jwtOperation),
                        UsernamePasswordAuthenticationFilter.class);
    }
}
