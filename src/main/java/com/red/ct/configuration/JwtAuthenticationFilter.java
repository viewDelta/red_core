package com.red.ct.configuration;

import com.red.ct.data.model.Base;
import com.red.ct.exception.InvalidTokenException;
import com.red.ct.utility.jwt.JwtOperation;
import com.red.ct.utility.constants.ApplicationCode;
import com.red.ct.utility.constants.Constant;
import com.red.ct.utility.constants.Header;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtOperation jwtOperation;

    public JwtAuthenticationFilter(JwtOperation jwtOperation){
        this.jwtOperation = jwtOperation;
    }

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        try {
            String correlationId = httpServletRequest.getHeader(Header.X_CORRELATION_ID.getValue());
            if(StringUtils.isEmpty(StringUtils.trimToNull(correlationId))) {
                MDC.put(Constant.CORRELATION_ID, UUID.randomUUID().toString());
            } else if(null != UUID.fromString(correlationId)){
                MDC.put(Constant.CORRELATION_ID, correlationId);
            }
            String authorizationHeader = httpServletRequest.getHeader(Header.AUTHORIZATION.getValue());
            if (StringUtils.isEmpty(authorizationHeader) || !authorizationHeader.startsWith(Constant.BEARER)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
                return;
            }
            UsernamePasswordAuthenticationToken token = createToken(authorizationHeader);
            SecurityContextHolder.getContext().setAuthentication(token);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (Exception ex) {
            throw ex;
        } finally {
            MDC.remove(Constant.CORRELATION_ID);
        }
    }

    /**
     * This will form a token request from Authroization token
     * @param authorizationHeader {@link String}
     * @return {@link UsernamePasswordAuthenticationToken}
     * @throws InvalidTokenException
     */
    private UsernamePasswordAuthenticationToken createToken(String authorizationHeader) throws InvalidTokenException {
        String token = authorizationHeader.replace(Constant.BEARER, "");
        Base user = jwtOperation.getUser(token);
        if(StringUtils.equals(user.getToken(), token) && user.isActive()){
            return new UsernamePasswordAuthenticationToken(user, null, null);
        }
        log.error(ApplicationCode.INVALID_OR_MISSING_AUTH_TOKEN.getMsg());
        throw new InvalidTokenException(ApplicationCode.INVALID_OR_MISSING_AUTH_TOKEN.getMsg());
    }
}
