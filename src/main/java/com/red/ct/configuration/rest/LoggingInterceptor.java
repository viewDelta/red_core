package com.red.ct.configuration.rest;

import com.red.ct.utility.constants.Constant;
import com.red.ct.utility.constants.Header;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Slf4j
public class LoggingInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] reqBody,
                                        ClientHttpRequestExecution ex) throws IOException {
        String correlationId = httpRequest.getHeaders().get(Header.X_CORRELATION_ID.getValue()).get(0);
        MDC.put(Constant.CORRELATION_ID, correlationId);
        log.info("Sending {} {} with body: {}", httpRequest.getMethod(),
                httpRequest.getURI(), new String(reqBody, StandardCharsets.UTF_8));
        ClientHttpResponse response = ex.execute(httpRequest, reqBody);
        InputStreamReader isr = new InputStreamReader(response.getBody(), StandardCharsets.UTF_8);
        String body = new BufferedReader(isr).lines().collect(Collectors.joining(""));
        log.info("Response received with status code : {} payload : {}", response.getRawStatusCode(), body);
        MDC.remove(Constant.CORRELATION_ID);
        return response;
    }

}
