package com.red.ct.configuration;

import com.red.ct.utility.validator.DateFormatValidator;
import org.modelmapper.*;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;

@Configuration
public class ApplicationConfig {

	/**
     * This will return the Validator Instance
     * @return {@link Validator}
     */
    @Bean
    public Validator getValidator(){
        //Create ValidatorFactory which returns validator
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        //It validates bean instances
        return factory.getValidator();
    }
    
    /**
     * This will return the ObjectMapper Instance
     * @return {@link ModelMapper}
     */
    @Bean
    public ModelMapper getMapper() {
    	ModelMapper mapper = new ModelMapper();
    	mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        
    	Provider<Date> localDateProvider = new AbstractProvider<Date>() {
            @Override
            public Date get() {
                return new Date();
            }
        };

        Converter<String, Date> toStringDate = new AbstractConverter<String, Date>() {
            @Override
            protected Date convert(String source) {
            	return DateFormatValidator.getDateFromString(source);
            }
        };
        mapper.createTypeMap(String.class, Date.class);
        mapper.addConverter(toStringDate);
        mapper.getTypeMap(String.class, Date.class).setProvider(localDateProvider);
    	return mapper;
    }
}
