package com.red.ct.configuration.mongo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Mongo DB collection configuration
 */
@Configuration
@EnableMongoRepositories(basePackages = "com.red.ct", mongoTemplateRef = "enrollmentMongoTemplate")
public class EnrollmentMongoCollection {

    @Value("${com.red.ct.collections.restaurant:restaurant_details}")
    private String restaurant_details_collection;

    @Value("${com.red.ct.collections.deliveryPerson:deliveryPerson_details}")
    private String deliveryPerson_details_collection;

    @Value("${com.red.ct.collections.customer:customer_details}")
    private String customer_details_collection;

    @Value("${com.red.ct.collections.serviceLocation:service_location}")
    private String service_location_collection;

    @Bean
    public String restaurantCollection() {
        return restaurant_details_collection;
    }

    @Bean
    public String deliveryPersonCollection() {
        return deliveryPerson_details_collection;
    }

    @Bean
    public String customerCollection() {
        return customer_details_collection;
    }

    @Bean
    public String serviceLocationCollection() {
        return service_location_collection;
    }

}
