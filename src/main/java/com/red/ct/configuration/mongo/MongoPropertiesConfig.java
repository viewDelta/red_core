package com.red.ct.configuration.mongo;

import lombok.Data;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * This cofigures the mongodb connection properties, to respective 'MongoProperties'.
 * Note : identifer name should be same
 */
@Data
@ConfigurationProperties(prefix = "spring.data.mongodb")
public class MongoPropertiesConfig {

    private MongoProperties enrollment = new MongoProperties();
    private MongoProperties grub = new MongoProperties();

}
