package com.red.ct.configuration.mongo;

import com.mongodb.client.MongoClients;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

/**
 * This class instantiate MongoTemplate, by using MongoPropertiesConfig object.
 * Primary database has been made to, enrollment as we need to autenticate every api request
 */
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MongoPropertiesConfig.class)
public class MongoConfig {

    private final MongoPropertiesConfig mongoProperties;

    @Primary
    @Bean(name = "enrollmentMongoTemplate")
    public MongoTemplate enrollmentMongoTemplate() throws Exception {
        return new MongoTemplate(enrollmentFactory(mongoProperties.getEnrollment()));
    }

    @Bean(name = "grubMongoTemplate")
    public MongoTemplate grubMongoTemplate() throws Exception {
        if(null != mongoProperties.getGrub().getUri())
            return new MongoTemplate(grubFactory(mongoProperties.getGrub()));
        else
            return null;
    }

    @Bean
    @Primary
    public MongoDatabaseFactory enrollmentFactory(final MongoProperties mongo) throws Exception {
        return new SimpleMongoClientDatabaseFactory(MongoClients.create(mongo.getUri()), mongo.getDatabase());
    }

    @Bean
    public MongoDatabaseFactory grubFactory(final MongoProperties mongo) throws Exception {
        if(null != mongoProperties.getGrub().getUri())
            return new SimpleMongoClientDatabaseFactory(MongoClients.create(mongo.getUri()), mongo.getDatabase());
        else
            return null;
    }
}
