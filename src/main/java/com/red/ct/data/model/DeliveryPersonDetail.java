package com.red.ct.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "#{@deliveryPersonCollection}")
public class DeliveryPersonDetail extends Base{
    private static final long serialVersionUID = 2055632796173450L;
    private String email;
    private String aadharNo;
    private String PAN;
    private String RC;
    private String licence;
    private boolean isReady;
}
