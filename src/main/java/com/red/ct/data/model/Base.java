package com.red.ct.data.model;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class Base implements Serializable {
    private static final long serialVersionUID = 2871127563268521774L;
    private String _id;
    private String phoneNo;
    private String uuid;
    private String name;
    private String token;
    private Instant tokenCreatedAt;
    private boolean isActive;
    private Instant createdAt;
    private Instant updatedAt;
    private String countryCode;
    private String stateCode;
    private String pinCode;
}
