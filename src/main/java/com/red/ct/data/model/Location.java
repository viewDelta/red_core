package com.red.ct.data.model;

import lombok.Data;

@Data
public class Location {
    private double longitude;
    private double latitude;
}
