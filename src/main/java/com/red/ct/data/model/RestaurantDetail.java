package com.red.ct.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "#{@restaurantCollection}")
public class RestaurantDetail extends Base{
    private static final long serialVersionUID = 7120742943218778545L;
    private String restaurantName;
    private String address;
    private String landmark;
    private String pan;
    private String email;
    private Location location;
    private boolean isReady;
}
