package com.red.ct.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Data
@Document(collection = "#{@serviceLocationCollection}")
public class ServiceLocation implements Serializable {
    private static final long serialVersionUID = 6106417785550133874L;
    private String _id;
    private String countryCode;
    private String stateCode;
    private List<String> servicePinCodes;
}
