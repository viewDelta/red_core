package com.red.ct.data.model;

import lombok.Data;

@Data
public class Label {
    private String key;
    private String address;
    private String landmark;
    private String pinCode;
    private Location location;
}
