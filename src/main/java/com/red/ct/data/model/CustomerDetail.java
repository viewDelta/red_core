package com.red.ct.data.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Document(collection = "#{@customerCollection}")
public class CustomerDetail extends Base{
    private static final long serialVersionUID = 2055632796173450L;
    private String email;
    private Date dob;
    private List<Label> labels;
}
