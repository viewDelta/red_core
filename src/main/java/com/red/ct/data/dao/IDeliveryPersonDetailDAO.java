package com.red.ct.data.dao;

import com.red.ct.data.model.CustomerDetail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.red.ct.data.model.DeliveryPersonDetail;

@Repository
public interface IDeliveryPersonDetailDAO extends MongoRepository<DeliveryPersonDetail, String> {

    DeliveryPersonDetail findByPhoneNo(String phoneNo);

    DeliveryPersonDetail findByUuid(String uuid);

    DeliveryPersonDetail findByPhoneNoOrEmailOrRCOrLicenceOrAadharNoOrPAN(String phoneNo, String email, String RC, String licence, String aadharNo, String Pan);

}
