package com.red.ct.data.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.red.ct.data.model.CustomerDetail;

@Repository
public interface ICustomerDetailDAO extends MongoRepository<CustomerDetail, String> {
	
	CustomerDetail findByPhoneNo(String phoneNo);
	
	CustomerDetail findByUuid(String uuid);
	
	CustomerDetail findByPhoneNoOrEmail(String phoneNo, String email);
}
