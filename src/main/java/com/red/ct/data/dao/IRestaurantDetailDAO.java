package com.red.ct.data.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.red.ct.data.model.RestaurantDetail;

import java.util.List;

@Repository
public interface IRestaurantDetailDAO extends MongoRepository<RestaurantDetail, String> {

    RestaurantDetail findByPhoneNo(String phoneNo);

    RestaurantDetail findByUuid(String uuid);

    RestaurantDetail findByPhoneNoOrEmailOrPan(String phoneNo, String email, String pan);

    List<RestaurantDetail> findByPinCode(String pinCode);

}
