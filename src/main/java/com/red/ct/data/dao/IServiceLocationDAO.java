package com.red.ct.data.dao;

import com.red.ct.data.model.ServiceLocation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IServiceLocationDAO extends MongoRepository<ServiceLocation, String> {
    ServiceLocation findByCountryCodeAndStateCode(String countryCode, String stateCode);
}
