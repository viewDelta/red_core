package com.red.ct.utility;

import com.red.ct.data.dao.ICustomerDetailDAO;
import com.red.ct.data.dao.IDeliveryPersonDetailDAO;
import com.red.ct.data.dao.IRestaurantDetailDAO;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.data.model.DeliveryPersonDetail;
import com.red.ct.data.model.RestaurantDetail;
import com.red.ct.utility.jwt.JwtOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JwtOperationTest {

    @Mock
    private ICustomerDetailDAO customerDetailDAO;

    @Mock
    private IRestaurantDetailDAO restaurantDetailDAO;

    @Mock
    private IDeliveryPersonDetailDAO deliveryPersonDetailDAO;

    @InjectMocks
    private JwtOperation jwtOperation;

    private CustomerDetail user;
    private HttpHeaders headers;
    private String generatedToken, name;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(jwtOperation, "jwtSecret","u7DxSI/2SpHclD+lhboOF69cHIkTmeSSwyopLRe3Wkmjtu8Sp3iC0+2mAUN3TdTnJs9Gh61Z56BTJ0938gGylz+d5cW8mk4wv6JdDm" +
                "jkkbEn89QWIoMcrVsPcGG1RBZIvL61vw6kLyMXM8UtYVbCzlAolEwuxbmNEfmzKlUjaep10Tr7Ka");
        user = new CustomerDetail();
        name = "Steve Austin";
        user.setName(name);
        user.setPhoneNo("+91000999900000");
        generatedToken = jwtOperation.getTokenForUser(user);
    }

    @Test
    public void test_getTokenForUser_whenTokenIsAlreadyPresentForUser() {
        String token = "tokenFromDB";
        user.setToken(token);
        Assert.assertEquals(token, jwtOperation.getTokenForUser(user));
    }

    @Test
    public void test_getTokenForUser_whenTokenIsNotPresentForUser() {
        RestaurantDetail user2 = new RestaurantDetail();
        user2.setName("Gold");
        user2.setPhoneNo("+91000000000000");
        DeliveryPersonDetail user3 = new DeliveryPersonDetail();
        user3.setName("Gold");
        user3.setPhoneNo("+91000000000000");
        Assert.assertNotNull(jwtOperation.getTokenForUser(user));
        Assert.assertNotNull(jwtOperation.getTokenForUser(user2));
        Assert.assertNotNull(jwtOperation.getTokenForUser(user3));
    }

    @Test
    public void test_getUser(){
        String name = "Gold", phNo = "000000000000";
        RestaurantDetail user2 = new RestaurantDetail();
        user2.setName(name);
        user2.setPhoneNo(phNo);
        String token = jwtOperation.getTokenForUser(user2);
        when(restaurantDetailDAO.findByPhoneNo(phNo)).thenReturn(user2);
        RestaurantDetail detail = (RestaurantDetail) jwtOperation.getUser(token);
        Assert.assertEquals(name, detail.getName());
        Assert.assertEquals(phNo, jwtOperation.getUser(token).getPhoneNo());

        DeliveryPersonDetail user3 = new DeliveryPersonDetail();
        user3.setName(name);
        user3.setPhoneNo(phNo);
        token = jwtOperation.getTokenForUser(user3);
        when(deliveryPersonDetailDAO.findByPhoneNo(anyString())).thenReturn(user3);
        Assert.assertEquals(name, jwtOperation.getUser(token).getName());
        Assert.assertEquals(phNo, jwtOperation.getUser(token).getPhoneNo());

        CustomerDetail user = new CustomerDetail();
        user.setName(name);
        user.setPhoneNo(phNo);
        token = jwtOperation.getTokenForUser(user);
        when(customerDetailDAO.findByPhoneNo(anyString())).thenReturn(user);
        Assert.assertEquals(name, jwtOperation.getUser(token).getName());
        Assert.assertEquals(phNo, jwtOperation.getUser(token).getPhoneNo());

        token = "xyz";
        Assert.assertNull(jwtOperation.getUser(token));
    }
}