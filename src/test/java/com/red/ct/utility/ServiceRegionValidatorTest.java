package com.red.ct.utility;

import com.red.ct.data.dao.IServiceLocationDAO;
import com.red.ct.data.model.ServiceLocation;
import com.red.ct.utility.validator.ServiceRegionValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceRegionValidatorTest {

    @Mock
    private IServiceLocationDAO serviceLocationDAO;

    @InjectMocks
    ServiceRegionValidator serviceRegionValidator;

    ServiceLocation serviceLocation = new ServiceLocation();
    String countryCode;
    String stateCode;
    String pinCode;

    @Before
    public void init() {
        serviceLocation.setCountryCode("IN");
        serviceLocation.setStateCode("OD");
        serviceLocation.setServicePinCodes(Arrays.asList("765001"));

    }

    @Test
    public void test_checkForServiceRegion_WhenServiceRegionIsValid() {
        countryCode = "IN";
        stateCode = "OD";
        pinCode = "765001";
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(serviceLocation);
        assertTrue(serviceRegionValidator.checkForServiceRegion(countryCode, stateCode, pinCode));
    }

    @Test
    public void test_checkForServiceRegion_WhenServiceRegionIsInValid() {
        countryCode = "IN";
        stateCode = "OD";
        pinCode = "765002";
        when(serviceLocationDAO.findByCountryCodeAndStateCode(anyString(), anyString())).thenReturn(serviceLocation);
        assertFalse(serviceRegionValidator.checkForServiceRegion(countryCode, stateCode, pinCode));
    }

}
