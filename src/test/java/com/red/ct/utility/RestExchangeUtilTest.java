package com.red.ct.utility;

import com.red.ct.domain.outbound.request.IRequest;
import com.red.ct.domain.outbound.response.IResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class RestExchangeUtilTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private RestExchangeUtil restExchangeUtil;

    @Test
    public void test_execute() throws InterruptedException {
        doReturn(mock(ResponseEntity.class)).when(restTemplate)
                .exchange(anyString(), any(HttpMethod.class) , any(HttpEntity.class), any(Class.class));
        assertNotNull(restExchangeUtil
                .execute("http://localohost", HttpMethod.POST, new HttpHeaders(), mock(IRequest.class), IResponse.class));
    }
}