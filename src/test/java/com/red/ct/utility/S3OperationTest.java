package com.red.ct.utility;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;

@RunWith(JUnit4.class)
public class S3OperationTest {

    @Mock
    private AmazonS3 s3client;

    private S3Operation s3Operation;
    private String bucketPath, localFile;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        s3Operation = new S3Operation();
        bucketPath = "my-bucket";
        localFile = "example.csv";
    }

    @Test(expected = Test.None.class)
    public void uploadFile_onSuccess() throws Exception {
        Mockito.when(s3client.putObject(any(PutObjectRequest.class))).thenReturn(Mockito.mock(PutObjectResult.class));
        s3Operation.uploadFile(bucketPath, localFile, s3client);
    }

    @Test(expected = Exception.class)
    public void uploadFile_onUploadFail() throws Exception {
        Mockito.when(s3client.putObject(any(PutObjectRequest.class))).thenThrow(Mockito.mock(SdkClientException.class));
        s3Operation.uploadFile(bucketPath, localFile, s3client);
    }

    @Test(expected =  Test.None.class)
    public void uploadFile_onEmptyFile() throws Exception {
        Mockito.when(s3client.putObject(any(PutObjectRequest.class))).thenThrow(Mockito.mock(SdkClientException.class));
        s3Operation.uploadFile(bucketPath, null, s3client);
    }
}