package com.red.ct.utility.constants;

import org.junit.Test;

import static org.junit.Assert.*;

public class CountryCodeTest {

    @Test
    public void isValidCodeTest() {
        assertTrue(CountryCode.isValidCode("IN"));
        assertFalse(CountryCode.isValidCode("US"));
    }
}