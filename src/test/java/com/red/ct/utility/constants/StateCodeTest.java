package com.red.ct.utility.constants;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class StateCodeTest {

    @Test
    public void isValidCodeTest() {
        assertTrue(StateCode.isValidCode("OD"));
        assertFalse(StateCode.isValidCode("XY"));
        assertFalse(StateCode.isValidCode(""));
        assertFalse(StateCode.isValidCode(null));
    }
}