package com.red.ct.utility.jwt;

import com.red.ct.data.model.Base;
import com.red.ct.data.model.CustomerDetail;
import com.red.ct.exception.InvalidTokenException;
import com.red.ct.utility.constants.Constant;
import com.red.ct.utility.constants.Header;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProxyJwtTest {

    @Mock
    private JwtOperation jwtOperation;

    @InjectMocks
    private ProxyJwt proxyJwt;

    @Test
    public void test_getTokenForUser_forValidUser(){
        when(jwtOperation.getTokenForUser(any(Base.class))).thenReturn("token");
        assertNotNull(proxyJwt.getTokenForUser(mock(CustomerDetail.class)));
    }

    @Test
    public void test_getTokenForUser_forNull(){
        assertNull(proxyJwt.getTokenForUser(null));
    }

    @Test(expected = InvalidTokenException.class)
    public void test_getUser_forNull() throws InvalidTokenException {
        proxyJwt.getUser(null);
    }

    @Test
    public void test_getUser_forValidToken() throws InvalidTokenException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(Header.AUTHORIZATION.getValue(), Constant.BEARER+"vhhfff");
        when(jwtOperation.getUser(anyString())).thenReturn(mock(Base.class));
        assertNotNull(proxyJwt.getUser(headers));
    }
}