package com.red.ct.utility;

import com.red.ct.utility.constants.ApplicationCode;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.*;

public class ResponseHelperTest {

    @Test
    public void test_buildSuccessResponse() {
        assertEquals(HttpStatus.OK.value(), ResponseHelper.buildSuccessResponse(HttpStatus.OK).getStatusCodeValue());
        assertEquals(HttpStatus.CREATED.value(), ResponseHelper.buildSuccessResponse(HttpStatus.CREATED).getStatusCodeValue());
    }

    @Test
    public void test_getCodeFromMsg_forValidErrorMsg() {
        assertEquals(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getCode(),
                ResponseHelper.getCodeFromMsg(ApplicationCode.REQUEST_PROCESSED_SUCCESSFULLY.getMsg()));
    }

    @Test
    public void test_getCodeFromMsg_forInvalidErrorMsg() {
        assertNull(ResponseHelper.getCodeFromMsg("xuz"));
    }
}