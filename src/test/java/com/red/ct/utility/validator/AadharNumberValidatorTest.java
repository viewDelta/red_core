package com.red.ct.utility.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AadharNumberValidatorTest {

    private AadharNumberValidator validator = new AadharNumberValidator();

    @Mock
    private ConstraintValidatorContext context;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValid_forValidAadharNumber() {
        assertTrue(validator.isValid("120032102302", context));
    }

    @Test
    public void isValid_forEmptyAadharNumber() {
        assertFalse(validator.isValid("", context));
    }

    @Test
    public void isValid_forInvalidAadharNumber() {
        assertFalse(validator.isValid("xyza1234Begx", context));
    }
}