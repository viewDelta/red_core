package com.red.ct.utility.validator;

import org.junit.Test;
import org.mockito.Mock;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

public class PANValidatorTest {

    private PANValidator PANValidator = new PANValidator();

    @Mock
    private ConstraintValidatorContext context;

    @Test
    public void isValid_whenPANisValid() {
        assertTrue(PANValidator.isValid("GHAUU0927T", context));
    }

    @Test
    public void isValid_whenPANisInValid() {
        assertFalse(PANValidator.isValid("", context));
        assertFalse(PANValidator.isValid("GHAUU09270", context));
    }
}