package com.red.ct.utility.validator;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DateFormatValidatorTest {

    @Test
    public void getDateFromString_whenFormatIsValid() {
        assertNotNull(DateFormatValidator.getDateFromString("01-02-1995 00:00:00"));
    }

    @Test
    public void getDateFromString_whenFormatIsInValid() {
        assertNull(DateFormatValidator.getDateFromString(""));
        assertNull(DateFormatValidator.getDateFromString("XTS"));
    }
}