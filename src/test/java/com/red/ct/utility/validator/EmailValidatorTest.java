package com.red.ct.utility.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class EmailValidatorTest {

    @Mock
    private ConstraintValidatorContext context;

    @Test
    public void isValid() {
        EmailValidator validator = new EmailValidator();
        assertTrue(validator.isValid("abc@gmail.com", context));
        assertTrue(validator.isValid("abc_65@gmail.com", context));
        assertFalse(validator.isValid("abc_65@@gmail.com", context));
        assertFalse(validator.isValid("", context));
    }
}