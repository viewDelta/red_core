package com.red.ct.utility.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

public class VehicleNumberValidatorTest {

    private VehicleNumberValidator validator = new VehicleNumberValidator();

    @Mock
    private ConstraintValidatorContext context;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValid_whenNumberIsValid() {
        assertTrue(validator.isValid("KA 01 TK 9210", context));
    }

    @Test
    public void isValid_whenNumberIsInValid() {
        assertFalse(validator.isValid("ZZ 01 TK 9210", context));
        assertFalse(validator.isValid("", context));
    }
}