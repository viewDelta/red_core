package com.red.ct.utility.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class NameValidatorTest {

    @Mock
    private ConstraintValidatorContext context;

    @Test
    public void isValid() {
        NameValidator validator = new NameValidator();
        assertTrue(validator.isValid( "Steve Austin" , context));
        assertFalse(validator.isValid( "123" , context));
        assertFalse(validator.isValid( "" , context));
        assertFalse(validator.isValid( "$#@" , context));
    }
}