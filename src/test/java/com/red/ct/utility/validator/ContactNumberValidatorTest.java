package com.red.ct.utility.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

public class ContactNumberValidatorTest {

    private ContactNumberValidator contactNumberValidator = new ContactNumberValidator();

    @Test
    public void testIsValid_whenPhoneNumberIsValid(){
        assertTrue(contactNumberValidator.isValid("+918093009870", "IN"));
    }

    @Test
    public void testIsValid_whenPhoneNumberIsInvalid(){
        assertFalse(contactNumberValidator.isValid("", "IN"));
        assertFalse(contactNumberValidator.isValid("abacbc", "IN"));
        assertFalse(contactNumberValidator.isValid("3009870", "IN"));
        assertFalse(contactNumberValidator.isValid("+918093009870", "CH"));
    }
}