package com.red.ct.utility.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

public class LicenseValidatorTest {

    private LicenseValidator licenseValidator = new LicenseValidator();

    @Mock
    private ConstraintValidatorContext context;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValid_whenLicenseIsValid() {
        assertTrue(licenseValidator.isValid("RJ-1320120123456",context));
        assertTrue(licenseValidator.isValid("RJ13 20120123456",context));
    }

    @Test
    public void isValid_whenLicenseIsInValid() {
        assertFalse(licenseValidator.isValid("",context));
        assertFalse(licenseValidator.isValid("xyz123",context));
    }
}