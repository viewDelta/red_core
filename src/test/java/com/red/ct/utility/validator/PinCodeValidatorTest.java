package com.red.ct.utility.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.*;

public class PinCodeValidatorTest {

    private PinCodeValidator pinCodeValidator = new PinCodeValidator();

    @Mock
    private ConstraintValidatorContext context;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isValid_whenNumberIsValid() {
        assertTrue(pinCodeValidator.isValid("765021", context));
    }

    @Test
    public void isValid_whenNumberIsInValid() {
        assertFalse(pinCodeValidator.isValid("1ba179", context));
        assertFalse(pinCodeValidator.isValid("", context));
    }
}