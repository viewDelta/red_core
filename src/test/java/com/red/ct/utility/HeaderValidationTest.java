package com.red.ct.utility;

import com.red.ct.configuration.key.AppChannelConfig;
import com.red.ct.configuration.key.KeyMappingConfig;
import com.red.ct.exception.InvalidHeaderValueException;
import com.red.ct.utility.constants.Header;
import com.red.ct.utility.constants.RequestType;
import com.red.ct.utility.constants.XChannel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HeaderValidationTest {

    @Mock
    private KeyMappingConfig keyMappingConfig;

    @Mock
    private AppChannelConfig appChannelConfig;

    @InjectMocks
    private HeaderValidation headerValidation;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void validateRequestHeaderTest_forSuccess() throws Exception {
        headers.set(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        headers.set(Header.X_API_KEY.getValue(), "becd91e8-6597-410d-ac36-ad4a112a2125");
        when(appChannelConfig.containsRequestTypeAndXChannel(anyString(), anyString())).thenReturn(true);
        when(keyMappingConfig.containsXChannelAndKey(anyString(), anyString())).thenReturn(true);
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN);
    }

    @Test(expected = InvalidHeaderValueException.class)
    public void validateRequestHeaderTest_forEmptyHeader() throws Exception {
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN);
    }

    @Test(expected = InvalidHeaderValueException.class)
    public void validateRequestHeaderTest_forInvalidApiKey() throws Exception {
        headers.set(Header.X_CHANNEL.getValue(), XChannel.BUYER.getValue());
        headers.set(Header.X_API_KEY.getValue(), "b1");
        when(appChannelConfig.containsRequestTypeAndXChannel(anyString(), anyString())).thenReturn(true);
        when(keyMappingConfig.containsXChannelAndKey(anyString(), anyString())).thenReturn(false);
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN);
    }

    @Test(expected = InvalidHeaderValueException.class)
    public void validateRequestHeaderTest_forInvalidXChannel() throws Exception {
        headers.set(Header.X_CHANNEL.getValue(), "B1");
        headers.set(Header.X_API_KEY.getValue(), "becd91e8-6597-410d-ac36-ad4a112a2125");
        when(appChannelConfig.containsRequestTypeAndXChannel(anyString(), anyString())).thenReturn(false);
        headerValidation.validateRequestHeader(headers, RequestType.LOGIN);
    }
}